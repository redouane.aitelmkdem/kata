package com.zsoft.kata.usecase.service;

import com.zsoft.kata.models.CommandItem;

import java.util.List;

public abstract class PriceCalculator {

    protected PriceCalculator next;

    public PriceCalculator addNext(PriceCalculator next) {
        this.next = next;
        return next;
    }

    public abstract List<CommandItem> calculate(List<CommandItem> items);

    public PriceCalculator getNext() {
        return next;
    }

    protected List<CommandItem> calculateNext(List<CommandItem> items){
        if (next == null) {
            return items;
        }
        return next.calculate(items);
    }
}
