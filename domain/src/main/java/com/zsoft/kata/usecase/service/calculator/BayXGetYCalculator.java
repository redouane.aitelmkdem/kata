package com.zsoft.kata.usecase.service.calculator;

import com.zsoft.kata.models.CommandItem;
import com.zsoft.kata.models.pricingtypes.BayXGetYPricingType;
import com.zsoft.kata.usecase.service.PriceCalculator;

import java.util.List;

public class BayXGetYCalculator extends PriceCalculator {

    @Override
    public List<CommandItem> calculate(List<CommandItem> items) {
        for(CommandItem commandItem : items) {
            if(commandItem.getPricingType() instanceof BayXGetYPricingType) {
                double cost = 0;
                BayXGetYPricingType pricingType = (BayXGetYPricingType) commandItem.getPricingType();
                int groupNbr = commandItem.getQuantity() / (pricingType.getX() + pricingType.getY());
                cost += groupNbr * pricingType.getX() * commandItem.getItem().getPrice();

                int addedItem = commandItem.getQuantity() - (groupNbr * (pricingType.getX() + pricingType.getY()));

                cost += addedItem * commandItem.getItem().getPrice();

                commandItem.setCost(cost);
            }
        }
        return calculateNext(items);
    }
}
