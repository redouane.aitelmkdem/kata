package com.zsoft.kata.usecase.service.calculator;

import com.zsoft.kata.models.CommandItem;
import com.zsoft.kata.usecase.service.PriceCalculator;

import java.util.List;

public class StandardPriceCalculator extends PriceCalculator {

    @Override
    public List<CommandItem> calculate(List<CommandItem> items) {
        for(CommandItem commandItem : items) {
            if(commandItem.getCost() == 0)
                commandItem.setCost(commandItem.getQuantity() * commandItem.getItem().getPrice());
        }
        return calculateNext(items);
    }
}
