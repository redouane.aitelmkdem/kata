package com.zsoft.kata.usecase;

import com.zsoft.kata.models.CommandItem;
import com.zsoft.kata.usecase.service.PriceCalculator;
import com.zsoft.kata.usecase.service.calculator.StandardPriceCalculator;

import java.util.*;

public class CommandUseCase {

    public double getCommandCost(List<CommandItem> items) {
        double price = 0.0;
        items = getPriceCalculator(items).calculate(items);
        for(CommandItem commandItem : items) {
            price += commandItem.getCost();
        }
        return price;
    }

    public double getCommandPrice(List<CommandItem> items) {
        double price = 0.0;
        PriceCalculator calculator = new StandardPriceCalculator();
        items = calculator.calculate(items);
        for(CommandItem commandItem : items) {
            price += commandItem.getCost();
        }
        return price;
    }

    private PriceCalculator getPriceCalculator(List<CommandItem> items) {
        Set<PriceCalculator> priceCalculators = new HashSet<>();
        for(CommandItem item : items) {
            priceCalculators.add(item.getPricingType().gePriceCalculator());
        }
        PriceCalculator finalCalculator = new StandardPriceCalculator();
        for(PriceCalculator calculator : priceCalculators) {
            PriceCalculator temp = finalCalculator;
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.addNext(calculator);
        }
        return finalCalculator;
    }
}
