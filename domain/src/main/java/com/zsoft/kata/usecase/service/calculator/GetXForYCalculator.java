package com.zsoft.kata.usecase.service.calculator;

import com.zsoft.kata.models.CommandItem;
import com.zsoft.kata.models.pricingtypes.GetXForYPricingType;
import com.zsoft.kata.usecase.service.PriceCalculator;

import java.util.List;

public class GetXForYCalculator extends PriceCalculator {

    @Override
    public List<CommandItem> calculate(List<CommandItem> items) {
        for(CommandItem commandItem : items) {
            if(commandItem.getPricingType() instanceof GetXForYPricingType) {

                double cost = 0;
                GetXForYPricingType pricingType = (GetXForYPricingType) commandItem.getPricingType();
                int groupNbr = commandItem.getQuantity() / pricingType.getX() ;
                cost += groupNbr * pricingType.getY();

                int addedItem = commandItem.getQuantity() - (groupNbr * pricingType.getX());
                cost += addedItem * commandItem.getItem().getPrice();
                commandItem.setCost(cost);

            }
        }
        return calculateNext(items);
    }
}
