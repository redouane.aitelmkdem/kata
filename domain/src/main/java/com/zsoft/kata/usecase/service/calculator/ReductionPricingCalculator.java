package com.zsoft.kata.usecase.service.calculator;

import com.zsoft.kata.models.CommandItem;
import com.zsoft.kata.models.pricingtypes.ReductionPricingType;
import com.zsoft.kata.usecase.service.PriceCalculator;

import java.util.List;

public class ReductionPricingCalculator extends PriceCalculator {

    @Override
    public List<CommandItem> calculate(List<CommandItem> items) {
        for(CommandItem commandItem : items) {
            if(commandItem.getPricingType() instanceof ReductionPricingType) {
                ReductionPricingType pricingType = (ReductionPricingType) commandItem.getPricingType();
                double itemCost = commandItem.getItem().getPrice() * (1 - pricingType.getReduction());
                commandItem.setCost(commandItem.getQuantity() * itemCost);
            }
        }
        return calculateNext(items);
    }
}
