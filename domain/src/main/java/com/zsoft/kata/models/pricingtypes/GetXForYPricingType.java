package com.zsoft.kata.models.pricingtypes;

import com.zsoft.kata.models.PricingType;
import com.zsoft.kata.usecase.service.PriceCalculator;
import com.zsoft.kata.usecase.service.calculator.GetXForYCalculator;

public class GetXForYPricingType implements PricingType {
    private final int x;
    private final double y;
    private static final PriceCalculator calculator = new GetXForYCalculator();

    @Override
    public PriceCalculator gePriceCalculator() {
        return calculator;
    }
    public GetXForYPricingType(int x, double y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
