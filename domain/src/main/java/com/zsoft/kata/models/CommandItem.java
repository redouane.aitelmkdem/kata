package com.zsoft.kata.models;

import lombok.*;

@Setter
@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@ToString
public class CommandItem {

    @NonNull
    private Item item;
    @NonNull
    private int quantity;
    @NonNull
    private PricingType pricingType;
    private double cost = 0;
}
