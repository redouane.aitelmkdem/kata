package com.zsoft.kata.models.pricingtypes;

import com.zsoft.kata.models.PricingType;
import com.zsoft.kata.usecase.service.PriceCalculator;
import com.zsoft.kata.usecase.service.calculator.StandardPriceCalculator;

public class StandardPricingType implements PricingType {
    private static final PriceCalculator calculator = new StandardPriceCalculator();
    @Override
    public PriceCalculator gePriceCalculator() {
        return calculator;
    }
}
