package com.zsoft.kata.models.pricingtypes;

import com.zsoft.kata.models.PricingType;
import com.zsoft.kata.usecase.service.PriceCalculator;
import com.zsoft.kata.usecase.service.calculator.ReductionPricingCalculator;

public class ReductionPricingType implements PricingType {
    private double reduction;
    private static final PriceCalculator calculator = new ReductionPricingCalculator();

    @Override
    public PriceCalculator gePriceCalculator() {
        return calculator;
    }
    public ReductionPricingType(double reduction) {
        this.reduction = reduction;
    }

    public double getReduction() {
        return reduction;
    }
}
