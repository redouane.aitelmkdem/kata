package com.zsoft.kata.models;

import com.zsoft.kata.usecase.service.PriceCalculator;

public interface PricingType {

    PriceCalculator gePriceCalculator();
}
