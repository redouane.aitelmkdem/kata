package com.zsoft.kata.models.pricingtypes;

import com.zsoft.kata.models.PricingType;
import com.zsoft.kata.usecase.service.PriceCalculator;
import com.zsoft.kata.usecase.service.calculator.BayXGetYCalculator;
import com.zsoft.kata.usecase.service.calculator.GetXForYCalculator;

public class BayXGetYPricingType implements PricingType {
    private final int x;
    private final int y;

    private static final PriceCalculator calculator = new BayXGetYCalculator();

    @Override
    public PriceCalculator gePriceCalculator() {
        return calculator;
    }
    public BayXGetYPricingType(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
