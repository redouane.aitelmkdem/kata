import com.zsoft.kata.models.CommandItem;
import com.zsoft.kata.models.Item;
import com.zsoft.kata.models.pricingtypes.BayXGetYPricingType;
import com.zsoft.kata.models.pricingtypes.GetXForYPricingType;
import com.zsoft.kata.models.pricingtypes.ReductionPricingType;
import com.zsoft.kata.models.pricingtypes.StandardPricingType;
import com.zsoft.kata.usecase.service.PriceCalculator;
import com.zsoft.kata.usecase.service.calculator.ReductionPricingCalculator;
import com.zsoft.kata.usecase.service.calculator.StandardPriceCalculator;
import com.zsoft.kata.usecase.service.calculator.BayXGetYCalculator;
import com.zsoft.kata.usecase.service.calculator.GetXForYCalculator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class PriceCalculatorTest {

    /**
     *  pour simplifier on renomme:
     *  - StandardCalculator : C1
     *  - BayXGetYCalculator : C2
     *  - GetXForYCalculator : C3
     *  - ReductionCalculator : C4
     */

    @Test
    void test_C1() {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", 2.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("B", 10.5), 1, new StandardPricingType()));

        PriceCalculator calculator = new StandardPriceCalculator();
        commandItems = calculator.calculate(commandItems);
        assertEquals(10, commandItems.get(0).getCost());
        assertEquals(10.5, commandItems.get(1).getCost());

    }
    @Test
    void test_C1_with_unused_calculator() {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", 2.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("B", 10.5), 1, new StandardPricingType()));

        PriceCalculator calculator = new StandardPriceCalculator();
        calculator.addNext(new BayXGetYCalculator());
        commandItems = calculator.calculate(commandItems);
        assertEquals(10, commandItems.get(0).getCost());
        assertEquals(10.5, commandItems.get(1).getCost());

    }

    @Test
    void test_C1_with_unused_pricing_type() {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", 2.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("B", 10.5), 1, new GetXForYPricingType(2,2.0)));

        PriceCalculator calculator = new StandardPriceCalculator();
        commandItems = calculator.calculate(commandItems);
        assertEquals(10, commandItems.get(0).getCost());
        assertEquals(10.5, commandItems.get(1).getCost());

    }

    private static Stream<Arguments> testC2Params() {
        return Stream.of(
                arguments(1.0, 8, 1, 0, 8),
                arguments(1.0, 8, 1, 1, 4),
                arguments(1.0, 7, 1, 1, 4),
                arguments(1.0, 8, 3, 1, 6),
                arguments(2.0, 8, 2, 1, 12),
                arguments(2.0, 5, 2, 1, 8)
        );
    }

    @ParameterizedTest
    @MethodSource("testC2Params")
    void test_C2_calculator(double price, int quantity, int bayX, int getY, double cost) {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", price), quantity, new BayXGetYPricingType(bayX, getY)));

        PriceCalculator calculator = new BayXGetYCalculator();
        commandItems = calculator.calculate(commandItems);
        assertEquals(cost, commandItems.get(0).getCost());

    }

    @Test
    void test_C1_and_C2() {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", 2.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("B", 10.5), 1, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("C", 2.0), 5, new BayXGetYPricingType(2, 1)));
        commandItems.add(new CommandItem(new Item("D", 1.0), 8, new BayXGetYPricingType(1, 1)));

        PriceCalculator calculator = new BayXGetYCalculator();
        calculator.addNext(new StandardPriceCalculator());
        commandItems = calculator.calculate(commandItems);
        assertEquals(10, commandItems.get(0).getCost());
        assertEquals(10.5, commandItems.get(1).getCost());
        assertEquals(8, commandItems.get(2).getCost());
        assertEquals(4, commandItems.get(3).getCost());
    }

    private static Stream<Arguments> testC3Params() {
        return Stream.of(
                arguments(1.0, 8, 1, 1.0, 8),
                arguments(1.0, 8, 2, 1.0, 4),
                arguments(1.0, 7, 2, 1.0, 4),
                arguments(2.0, 8, 3, 5.0, 14),
                arguments(2.0, 5, 2, 3.0, 8)
        );
    }

    @ParameterizedTest
    @MethodSource("testC3Params")
    void test_C3_calculator(double price, int quantity, int bayX, double forY, double cost) {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("C", price), quantity, new GetXForYPricingType(bayX, forY)));

        PriceCalculator calculator = new GetXForYCalculator();
        calculator.addNext(new StandardPriceCalculator());
        commandItems = calculator.calculate(commandItems);
        assertEquals(cost, commandItems.get(0).getCost());
    }

    @Test
    void test_C1_and_C2_and_C3() {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", 2.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("B", 10.5), 1, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("C", 2.0), 5, new BayXGetYPricingType(2, 1)));
        commandItems.add(new CommandItem(new Item("D", 1.0), 8, new BayXGetYPricingType(1, 1)));
        commandItems.add(new CommandItem(new Item("D", 2.0), 8, new GetXForYPricingType(3, 5.0)));

        PriceCalculator calculator = new BayXGetYCalculator();
        calculator.addNext(new GetXForYCalculator())
                .addNext(new StandardPriceCalculator());
        commandItems = calculator.calculate(commandItems);
        assertEquals(10, commandItems.get(0).getCost());
        assertEquals(10.5, commandItems.get(1).getCost());
        assertEquals(8, commandItems.get(2).getCost());
        assertEquals(4, commandItems.get(3).getCost());
        assertEquals(14, commandItems.get(4).getCost());
    }

    private static Stream<Arguments> testC4Params() {
        return Stream.of(
                arguments(4.0, 3, 0, 12),
                arguments(2.0, 8, 0.5, 8),
                arguments(1.0, 8, 0.25, 6),
                arguments(1.0, 8, 0.75, 2),
                arguments(2.0, 8, 1, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("testC4Params")
    void test_C4_calculator(double price, int quantity, double reduction, double cost) {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", price), quantity, new ReductionPricingType(reduction)));

        PriceCalculator calculator = new ReductionPricingCalculator();
        commandItems = calculator.calculate(commandItems);
        assertEquals(cost, commandItems.get(0).getCost());

    }

}
