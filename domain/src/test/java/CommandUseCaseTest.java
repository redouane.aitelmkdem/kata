import com.zsoft.kata.models.CommandItem;
import com.zsoft.kata.models.Item;
import com.zsoft.kata.models.pricingtypes.BayXGetYPricingType;
import com.zsoft.kata.models.pricingtypes.GetXForYPricingType;
import com.zsoft.kata.models.pricingtypes.ReductionPricingType;
import com.zsoft.kata.models.pricingtypes.StandardPricingType;
import com.zsoft.kata.usecase.CommandUseCase;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CommandUseCaseTest {

    @Test
    void command_price_test() {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", 2.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("B", 10.5), 1, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("D", 1.0), 8, new BayXGetYPricingType(1, 1)));
        commandItems.add(new CommandItem(new Item("B", 10.5), 1, new GetXForYPricingType(2,2.0)));
        commandItems.add(new CommandItem(new Item("A", 20), 1, new ReductionPricingType(1.2)));

        CommandUseCase commandUseCase = new CommandUseCase();
        assertEquals(59, commandUseCase.getCommandPrice(commandItems));
    }

    @Test
    void command_cost_test() {
        List<CommandItem> commandItems = new ArrayList<>();
        commandItems.add(new CommandItem(new Item("A", 2.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("B", 1.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("C", 3.0), 5, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("B", 10.5), 1, new StandardPricingType()));
        commandItems.add(new CommandItem(new Item("D", 2.0), 8, new GetXForYPricingType(3, 5.0)));
        commandItems.add(new CommandItem(new Item("C", 2.0), 5, new BayXGetYPricingType(2, 1)));
        commandItems.add(new CommandItem(new Item("D", 1.0), 8, new BayXGetYPricingType(1, 1)));
        commandItems.add(new CommandItem(new Item("E", 10.0), 4, new ReductionPricingType(0.5)));

        CommandUseCase commandUseCase = new CommandUseCase();
        assertEquals(86.5, commandUseCase.getCommandCost(commandItems));
    }
}
